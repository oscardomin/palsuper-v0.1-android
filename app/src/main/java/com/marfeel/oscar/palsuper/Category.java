package com.marfeel.oscar.palsuper;

/**
 * Created by oscar on 30/06/2014.
 */
public class Category {

    /* Main Category Types */
    public enum CategoryType {
        FRESCOS,
        CHARCUTERIA_QUESOS,
        ALIMENTACION,
        DESAYUNO_DULCES_PAN,
        LACTEOS_HUEVOS,
        PRODUCTOS_SIN_GLUTEN,
        CONGELADOS_HELADOS,
        BEBIDAS,
        BEBE,
        PERFUMERIA_HIGIENE,
        DROGUERIA_LIMPIEZA,
        MASCOTAS;
    }

    /* Subcategories Level 1 */
    public enum FreshSubcategory {
        CARNE_AVES,
        PESCADO_MARISCO,
        FRUTA,
        VERDURA_HORTALIZAS;
    }

    public enum MeatAndCheeseSubcategory {
        COCIDO,
        CURADO,
        QUESOS,
        PATES_FOIE_HUNTABLES,
        SALCHICHAS;
    }

    public enum GrocerySubcategory {
        ACEITE,
        ACEITUNAS_ENCURTIDOS,
        ARROZ,
        CALDOS_SOPAS_PURES_GAZPACHO,
        CONSERVAS_DULCES_MERMELADAS,
        CONSERVAS_PESCADO,
        CONSERVAS_VEGETALES,
        HARINAS,
        LEGUMBRES,
        PASTAS,
        PATATAS_FRITAS_APERITIVOS_FRUTOS_SECOS,
        PIZZA_BASES_MASAS,
        PLATTOS_PREPARADOS,
        PLATOS_PREPARADOS_EN_SOBRE,
        PLATOS_PREPARADOS_REFRIGERADOS,
        SAL_ESPECIAS_SAZONADORES,
        SALSAS,
        VINAGRE_ALIÑOS,
        DIETETICOS,
        INTERNACIONAL;
    }

    public enum BreakfastBreadSubcategory {
        AZUCAR_EDULCORANTES,
        BOLLERIA_INSDUSTRIAL,
        CACAO,
        CAFE,
        CEREALES,
        CHOCOLATES_BOMBONES,
        CARAMELOS_CHICLES,
        GALLETAS,
        INFUSIONES,
        PAN,
        PREPARACION_DE_POSTRES;
    }

    public enum DairyEggsSubcategory {
        BATIDOS,
        HORCHATAS,
        LECHE,
        MANTEQUILLA_MARGARINA,
        NATA,
        POSTRES,
        YOGURES,
        HUEVOS;
    }

    public enum FreezeSubcategory {
        HELADOS,
        PESCAD_MARISCO,
        VERDURA_FRUTA,
        CARNE_POLLO,
        PLATOS_PRECOCINADOS,
        PLANIFICACION_REPOSTERIA;
    }

    public enum DrinksSubcategory {
        AGUAS,
        REFRESCOS,
        GASEOSAS_SODAS,
        ISOTONICAS_ENERGETICAS,
        ZUMOS_NECTARES,
        CERVEZAS,
        ALCOHOLES_LICORES,
        SANGRIAS_COMBINADOS_BASE_VINO,
        FINOS_DULCES,
        VINOS_BLANCOS,
        VINOS_ROSADOS,
        VINOS_TINTOS,
        CAVAS_CHAMPAGNE,
        SIDRA;
    }

    public enum BabySubcategory {
        ALIMENTACION_INFANTIL,
        HIGIENE,
        PAÑALES;
    }

    public enum PerfumeryHygieneSubcategory {
        AFEITADO,
        BOTIQUIN,
        COLONIAS,
        COSMETICA,
        CUIDADO_CABELLO,
        CUIDADO_CORPORAL,
        CUIDADO_FACIAL,
        CUIDADO_MANOS,
        CUIDADO_PIES,
        DEPILACION,
        DESODORANTE,
        GEL_DE_BAÑO,
        HIGIENE_BUCAL,
        HIGIENE_INTIMA,
        HIGIENE_SEXUAL;
    }


    public enum CleaningSubcategory {
        ACCESORIOS_LIMPIEZA,
        AMBIENTADORES,
        CERILLAS_MECHEROS,
        BOLSAS_BASURA_REUTILIZABLE,
        CONSERVACION_ALIMENTOS,
        CALZADO,
        CELULOSA,
        CUIDADO_ROPA_DETERGENTE,
        CUIDADO_ROPA_SUAVIZANTE,
        CUIDADO_ROPA_COMPLEMENTOS,
        INSECTICIDAS,
        LAVAVAJILLAS,
        LIMPIADORES_PARA_EL_HOGAR,
        PILAS_BOMBILLAS;
    }

    public enum PetsSubcategory {
        GATOS,
        PERROS,
        RESTO_ANIMALES;
    }

    private final CategoryType categoryName;
    private final Category[] subcategories;
    private final ItemCompra[] products;

    public Category(CategoryType categoryName, Category[] subcategories, ItemCompra[] products) {
        this.categoryName = categoryName;
        this.subcategories = subcategories;
        this.products = products;
    }

    /*private Category[] generateSubcaegories(CategoryType categ) {

    }*/
}
