package com.marfeel.oscar.palsuper;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;


/**
 * Created by oscar on 26/06/2014.
 */
public class ListItems extends SherlockActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.marfeel.oscar.palsuper.R.layout.itemlist);

        String[] myStringArrayItems = new String[20];

        for(int i = 0; i < myStringArrayItems.length; i++) {
            myStringArrayItems[i] = "Product " + i;
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, myStringArrayItems);
        ListView listView = (ListView) findViewById(com.marfeel.oscar.palsuper.R.id.listViewProducts);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(mMessageClickedHandler);

    }

    // When you tap a list element this function is called
    protected AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View v, int position, long id) {

            Context context = getApplicationContext();
            String selectedItem = ((TextView)v).getText().toString();
            CharSequence messagePopUp = "Product "+ selectedItem + " selected!" ;
            int duration = Toast.LENGTH_SHORT;

            Intent i = new Intent(ListItems.this, ItemDetails.class);
            startActivity(i);
            finish();

            Toast toast = Toast.makeText(context, messagePopUp, duration);
            toast.show();
        }
    };

}
