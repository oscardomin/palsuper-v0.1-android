package com.marfeel.oscar.palsuper;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;

import java.util.Vector;


public class MyActivity extends SherlockFragmentActivity implements ActionBar.OnNavigationListener {


    private static final int LIST_TYPE_BUY_LIST = 0;
    private static final int LIST_TYPE_PRODUCT_LIST = 1;
    private BuyListDataBase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.marfeel.oscar.palsuper.R.layout.activity_my);

        // Load items of ListView
        paintList(LIST_TYPE_BUY_LIST);

        //Customize ActionBar
        setActionBar();
    }

    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        //mSelected.setText("Selected: " + mLocations[itemPosition]);
        return true;
    }

    public void setActionBar() {
        com.actionbarsherlock.app.ActionBar actionBar = getSupportActionBar();

        //TODO: Set a customized font to the title of the ActionBar

        //Customizing ActionBar
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.customactionbar);
    }

    private void paintList(int type_of_list) {

        db = new BuyListDataBase(this);
        //db.resetDB();

        Vector<String> v = new Vector<String>();

        switch(type_of_list) {
            case LIST_TYPE_BUY_LIST:
                //resetTable(db, "buylists");
                v = db.selectAll("buylists");
                break;

            case LIST_TYPE_PRODUCT_LIST:
                //resetTable(db, "products");
                v = db.selectAll("products");
                break;
            default:
                break;
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.cell_list, R.id.listcell, v);
        MyFragment myFragment = (MyFragment) getSupportFragmentManager().findFragmentById(R.id.myfragment);
        ListView listView = myFragment.getListView();
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(mMessageClickedHandler);
    }

    private void resetTable(String dbname) {
        if(dbname.equals("buylists")) {
            db.deleteAll(dbname);

            db.save(dbname, "Cumpleanyos de Jose");
            db.save(dbname, "Lista 12-7-2014");
            db.save(dbname, "Lista 5-7-2014");
            db.save(dbname, "Lista 28-6-2014");
            db.save(dbname, "Lista 14-6-2014");
            db.save(dbname, "Lista 7-6-2014");
            db.save(dbname, "Lista 31-5-2014");
            db.save(dbname, "Fiesta Calafell");
            db.save(dbname, "Cumple àvia");
        }
        else if (dbname.equals("products")) {
            db.deleteAll(dbname);

            db.save(dbname, "Coca-Cola");
            db.save(dbname, "Fanta Naranja");
            db.save(dbname, "Fanta Limón");
            db.save(dbname, "Aquarius");
            db.save(dbname, "Nestea");
            db.save(dbname, "Estrella Damm");
            db.save(dbname, "Powerade");
            db.save(dbname, "Coca-Cola Zero");
        }
    }

    // When you tap a list element this function is called
    protected AdapterView.OnItemClickListener mMessageClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView parent, View v, int position, long id) {
    //TODO: Replace the fragment of the buy-lists with product-list

            //Toast message to detect which element has been tapped
            Context context = getApplicationContext();
            CharSequence text = "Item number "+position+" tapped!";
            int duration = Toast.LENGTH_SHORT;

            Toast toast = Toast.makeText(context, text, duration);
            toast.show();


            //Replacing the current fragment with a new one
            MyFragment newFragment = new MyFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            Vector<String> vec = new Vector<String>();
            //resetTable("products");
            vec = db.selectAll("products");


            ArrayAdapter adapter = new ArrayAdapter<String>(context,
                    R.layout.cell_list, R.id.listcell, vec);

            newFragment.setListAdapter(adapter);


            // Replace whatever is in the myfragment view with this fragment,
            // and add the transaction to the back stack so the user can navigate back
            transaction.replace(com.marfeel.oscar.palsuper.R.id.myfragment, newFragment);
            transaction.addToBackStack(null);



            // Commit the transaction
            transaction.commit();

        }
    };

}
