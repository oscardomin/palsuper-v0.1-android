package com.marfeel.oscar.palsuper;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Vector;

/**
 *  Created by Óscar on 15/07/2014.
 */

public class BuyListDataBase extends SQLiteOpenHelper {

    public BuyListDataBase (Context context) {
        super (context, "base_de_datos", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE buylists (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, name TEXT NOT NULL);");
        db.execSQL("CREATE TABLE products (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, name TEXT NOT NULL);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i2) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS buylists");
        db.execSQL("DROP TABLE IF EXISTS products");

        // create fresh books table
        this.onCreate(db);
    }

    public void resetDB() {
        dropTable("buylists");
        dropTable("products");
        createTable("buylists");
        createTable("products");
    }

    private void createTable(String tableName) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("CREATE TABLE "+tableName+" (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, name TEXT NOT NULL);");
    }

    private void dropTable(String tableName) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS "+tableName);
    }

    public void save (String table, String name) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("INSERT INTO "+table+" (name) VALUES ('"+name+"');");
        db.close();
    }

    public void delete (String table, String name) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(table, "name='"+name+"'", null);
        //db.execSQL("DELETE FROM buylists WHERE name="+name);
        db.close();
    }

    public void deleteAll (String table) {
        SQLiteDatabase db = getWritableDatabase();
        db.delete(table, null, null);
        //db.execSQL("DELETE * FROM "+table);
        db.close();
    }

    public Vector<String> selectAll (String table) {
        Vector<String> result = new Vector<String>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT name FROM "+table, null);
        while(cursor.moveToNext()) {
            result.add(cursor.getString(0));
        }
        cursor.close();
        db.close();
        return result;
    }
}