package com.marfeel.oscar.palsuper;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockListFragment;

/**
 * Created by oscar on 09/07/2014.
 */
public class MyFragment extends SherlockListFragment {

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(com.marfeel.oscar.palsuper.R.layout.itemlist, container, false);
        return view;
    }
}
