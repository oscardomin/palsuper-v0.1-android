package com.marfeel.oscar.palsuper;

import android.os.Bundle;

import com.actionbarsherlock.app.SherlockActivity;

/**
 * Created by oscar on 30/06/2014.
 */
public class ItemDetails extends SherlockActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(com.marfeel.oscar.palsuper.R.layout.itemdetails);
    }
}
